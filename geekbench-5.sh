#!/bin/bash

sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install libc6:i386 libstdc++6:i386 -y

wget http://cdn.geekbench.com/Geekbench-5.0.3-Linux.tar.gz ~/

tar -zxvf ./Geekbench-5.0.3-Linux.tar.gz && cd ./Geekbench-5.0.3-Linux/

./geekbench5
